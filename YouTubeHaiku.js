var request = new XMLHttpRequest();
var url = "https://www.reddit.com/r/youtubehaiku/top.json?t=day&limit=10&count=10";

function getEmbedURL(html) {
	html = html.split(" ");
	for (var i = 0; i < html.length; i++) {
		if (html[i].indexOf("src")==0) {
			url = html[i].substring(4);
			url = url.substring(1, url.length-2);
			return url;
		}
	}
	return "";
}

request.onreadystatechange = function() {
	if (request.readyState == 4 && request.status == 200) {
		var parsedJSON = JSON.parse(request.responseText);
		var i;
		for (i = 0; i < parsedJSON.data.children.length; i ++) {
			var embedHtml = parsedJSON.data.children[i].data.media.oembed.html;
			var embedURL = getEmbedURL(embedHtml);
			if (embedURL==null) {
				continue;
			}
			var vidDiv = document.createElement("div");
			var iframe = document.createElement("iframe");
			iframe.width = 842;
			iframe.height = 480;
			iframe.setAttribute("frameborder", "0");
			iframe.setAttribute("allowfullscreen", "true");
			iframe.src = embedURL;
			document.getElementById("vidArea").appendChild(iframe);
			document.getElementById("vidArea").appendChild(document.createElement("br"));
			document.getElementById("vidArea").appendChild(document.createElement("br"));
			var permalinkUrl = "https://www.reddit.com" + parsedJSON.data.children[i].data.permalink;
			var permalink = document.createElement("a");
			permalink.href = permalinkUrl;
			permalink.text = "See post on reddit.";
			permalink.target = "_blank"
			document.getElementById("vidArea").appendChild(permalink);
			document.getElementById("vidArea").appendChild(document.createElement("br"));
			document.getElementById("vidArea").appendChild(document.createElement("br"));
		}
	}
}

request.open("GET", url, true);
request.send();
